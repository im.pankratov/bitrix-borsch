#!/bin/bash

rm -f /etc/php.d/*xdebug.ini*

echo 'zend_extension = xdebug.so
xdebug.remote_enable = On
xdebug.remote_connect_back = On' >> /etc/php.d/15-xdebug.ini.disabled

mv -f /etc/php.d/15-xdebug.ini.disabled /etc/php.d/15-xdebug.ini

echo -e "\e[1;32mRestarting apache service\e[0m"
service httpd restart

mkdir -p /tmp/php_sessions/www
mkdir -p /tmp/php_upload/www

wget --quiet --directory-prefix=/home/bitrix/www http://www.1c-bitrix.ru/download/scripts/bitrixsetup.php
wget --quiet --directory-prefix=/home/bitrix/www http://www.1c-bitrix.ru/download/scripts/restore.php

chown -R bitrix:bitrix /home/bitrix
chown -R bitrix:bitrix /tmp/php_sessions
chown -R bitrix:bitrix /tmp/php_upload

echo '
EnableSendfile Off
' >> /etc/httpd/bx/custom/z_bx_custom.conf

echo '
sendfile off;
' >> /etc/nginx/bx/conf/bitrix_general.conf

# cachefilesd installation
echo -e "\e[1;32mInstalling cachefilesd package\e[0m"
yum install -y cachefilesd
echo 'RUN=yes' >> /etc/default/cachefilesd


# Mail installation
echo -e "\e[1;32mInstalling mailx package\e[0m"
yum install -y mailx

# Set default msmtp account
MSMTP='/home/bitrix/.msmtprc'

rm -f $MSMTP
echo 'account default
logfile /home/bitrix/www/msmtp_default.log
host 127.0.0.1
port 25
from bitrix@localhost.localdomain
keepbcc on
auth off
tls off
tls_certcheck off
' >> $MSMTP

chown bitrix:bitrix $MSMTP
chmod 600 $MSMTP

# Set default bitrix user password = vagrant
BPASSWD='vagrant'
echo -e "$BPASSWD\n$BPASSWD" | (passwd --stdin bitrix)
