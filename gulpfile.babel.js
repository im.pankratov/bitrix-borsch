'use strict';

import plugins  from 'gulp-load-plugins';
import yargs    from 'yargs';
import browser  from 'browser-sync';
import gulp     from 'gulp';
import rimraf   from 'rimraf';
import yaml     from 'js-yaml';
import fs       from 'fs';
import proc     from 'child_process';
import crBump   from 'conventional-recommended-bump';
import pkg      from './package.json';

// Load all Gulp plugins into one variable
const $ = plugins();

// Flags
const SUSPEND    = !!(yargs.argv.suspend);

// Load settings from settings.yml
const {FOLDERS, CONVENTION} = loadConfig('config.yml');
const {DEV} = loadConfig('servers.yml');

const PATH = {
  server: {
    root:       DEV.www.host,
    local:      DEV.www.host + FOLDERS.local,
    watch:      [DEV.www.host, '!' + DEV.www.host + '{bitrix,upload,local,desktop_app}']
  },
  src:          {
    root:       FOLDERS.src
  }
};

function loadConfig(file) {
  let ymlFile = fs.readFileSync(file, 'utf8');
  return yaml.load(ymlFile);
}

// Execute child process with command
function execute(command, done) {
  proc.exec(command, (error, stdout, stderr) => {
    if (error) {
      console.error(`exec error: ${error}`);
      return done();
    }

    console.log(`stdout: ${stdout}`);
    if (stderr) {
      console.log(`stderr: ${stderr}`);
    }
    return done();
  });
}

// Task chains
const serve      = gulp.series(clean, copy, server, watch);
const release    = gulp.series(getVersion, bumpVersion, updateChangelog, commitChanges, tagVersion);

// Either serve on test Bitrix environment or just create dist archive
gulp.task('default', serve);
gulp.task('release', release);

function clean(done) {
  rimraf(PATH.server.local, done);
}

// TODO: remove this already?
// Virtual machine controlling task
gulp.task('vm', (done) => {
  if (SUSPEND) {
    return vmSuspend(done);
  }
  return vmUp(done);
});

function vmUp(done) {
  var command = 'vagrant up';
  execute(command, done);
}

function vmSuspend(done) {
  var command = 'vagrant suspend';
  execute(command, done);
}

// Copies files from "components" folder
function copy() {
  return gulp.src(PATH.src.components + '**/*', {dot: true})
    .pipe($.changed(PATH.server.components))
    .pipe(gulp.dest(PATH.server.components));
}

function getVersion(done) {
  crBump({
    preset: CONVENTION.preset
  }, (err, result) => {
    pkg.releaseType = result.releaseType;
    done();
  });
}

function bumpVersion() {
  return gulp.src(['./package.json', './bower.json'])
    .pipe($.bump({
      type: pkg.releaseType
    }))
    .pipe(gulp.dest('./'));
}

function updateChangelog() {
  return gulp.src('CHANGELOG.md', {
    buffer: false
  })
    .pipe($.conventionalChangelog({
      preset: CONVENTION.preset
    }))
    .pipe(gulp.dest('./'));
}

function commitChanges() {
  return gulp.src('.')
    .pipe($.git.add())
    .pipe($.git.commit('Bumped version number'));
}

function tagVersion() {
  return gulp.src('./package.json')
    .pipe($.tagVersion());
}

// Start a server with BrowserSync to preview the site in
function server(done) {
  browser.init({
    proxy: DEV.name,
    port: DEV.port,
    online: false
  });
  done();
}

// Reload the browser with BrowserSync
function reload(done) {
  browser.reload();
  done();
}

// Watch for changes to static assets, pages, Sass, and JavaScript
function watch() {
  gulp.watch(PATH.src.root, gulp.series(copy, reload));
  gulp.watch(PATH.server.watch, reload);
}
